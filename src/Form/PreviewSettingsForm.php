<?php

namespace Drupal\preview\Form;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Preview configuration form.
 */
class PreviewSettingsForm extends ConfigFormBase {

  public const SETTINGS = 'preview.settings';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The entity type bundle service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  private $bundleInfo;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->bundleInfo = $container->get('entity_type.bundle.info');
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'preview_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['settings'] = [
      '#tree' => TRUE,
    ];

    $config = $this->config(self::SETTINGS)->get('enabled') ?? [];

    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $entity_type_id = $definition->id();
        // We already have that for nodes.
        if ($entity_type_id === 'node') {
          continue;
        }

        $form['settings'][$entity_type_id] = [
          '#type' => 'container',
        ];
        $form['settings'][$entity_type_id]['enabled'] = [
          '#type' => 'checkbox',
          '#title' => $definition->getLabel(),
          '#default_value' => !empty($config[$entity_type_id]),
        ];
        $form['settings'][$entity_type_id]['bundles'] = [
          '#type' => 'details',
          '#open' => TRUE,
          '#title' => $this->t('@label settings', [
            '@label' => $definition->getLabel(),
          ]),
          '#states' => [
            'visible' => [
              ':input[name="settings[' . $entity_type_id . '][enabled]"]' => ['checked' => TRUE],
            ],
          ],
        ];
        $bundle_info = $this->bundleInfo->getBundleInfo($definition->id());
        foreach ($bundle_info as $bundle_id => $bundle_data) {
          $form['settings'][$entity_type_id]['bundles'][$bundle_id] = [
            '#type' => 'container',
          ];
          $form['settings'][$entity_type_id]['bundles'][$bundle_id]['enabled'] = [
            '#type' => 'checkbox',
            '#title' => $bundle_data['label'],
            '#default_value' => !empty($config[$entity_type_id][$bundle_id]),
          ];
          $form['settings'][$entity_type_id]['bundles'][$bundle_id]['default_view_mode'] = [
            '#type' => 'radios',
            '#title' => $this->t('Default view mode'),
            '#options' => ['default' => $this->t('Default')] + $this->entityDisplayRepository->getViewModeOptionsByBundle($definition->id(), $bundle_id),
            '#default_value' => !empty($config[$entity_type_id][$bundle_id]) ? $config[$entity_type_id][$bundle_id] : NULL,
            '#states' => [
              'visible' => [
                ':input[name="settings[' . $entity_type_id . '][bundles][' . $bundle_id . '][enabled]"]' => ['checked' => TRUE],
              ],
            ],
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_data = $form_state->getValue('settings');
    $config_data = [];
    foreach ($form_data as $entity_type_id => $type_data) {
      if (!$type_data['enabled']) {
        continue;
      }

      foreach ($type_data['bundles'] as $bundle_id => $bundle_data) {
        if (!$bundle_data['enabled'] || empty($bundle_data['default_view_mode'])) {
          continue;
        }
        $config_data[$entity_type_id][$bundle_id] = $bundle_data['default_view_mode'];
      }
    }

    $config = $this->config(self::SETTINGS);
    $config->set('enabled', $config_data);
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
