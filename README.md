# Preview Module

The Preview module allows you to preview selected entities in any view mode before saving. This module is based on the core Node module's preview functionality and extends it to other content entity types.

## Features

- Configure which entity types and bundles are subject to preview.
- Switch view modes during preview.
- Back link to return to the editing form from the preview page.

## Requirements

- Drupal 9.1 or higher

## Installation

1. Download and place the module in the `modules/contrib` directory.
2. Enable the module using the following Drush command:
    ```sh
    drush en preview
    ```

## Configuration

1. Navigate to the configuration page at `/admin/config/content/preview`.
2. Select the entity types and bundles you want to enable for preview.
3. Configure the default view mode for each bundle.

## Usage

1. When editing an entity of a configured type, you will see a "Preview" button.
2. Click the "Preview" button to see a preview of the entity in the selected view mode.
3. Use the "Back to editing" link to return to the editing form.

## Hooks

- `hook_help()`: Provides help text for the module.
- `hook_page_top()`: Adds a "Back to content editing" link on the preview page.
- `hook_form_alter()`: Alters entity forms to add the preview functionality.

## Services

- `preview.form_service`: Alters entity forms to add the preview functionality.
- `access_check.entity.preview`: Checks access to the entity preview page.
- `entity_preview`: Provides upcasting for a node entity in preview.

## Events

- `preview.back_link`: Fired when the preview back link is generated.